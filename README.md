# play ground for gitlab.pasteur.fr + virtualbox runner integration

# reference:
- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/executors/virtualbox.md

# pending issue (imho):
- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1121
- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/merge_requests/110

so far the service is running as root on the virtualbox host! ignoring the --user gitlab-runner flag

-> can not use the 
```
sudo gitlab-runner start
```

-> workaround:
```
[gitlab-runner@e6420 ~]$ gitlab-runner run
```

```
[gitlab-runner@e6420 ~]$ cat ~/.gitlab-runner/config.toml
concurrent = 1
check_interval = 0

[[runners]]
  name = "gitlab-runner@szut.bis.pasteur.fr"
  url = "https://gitlab.pasteur.fr/ci"
  token = "XXXXXXXXXXXXXXXXXXXXXX"
  executor = "virtualbox"
  [runners.cache]
  [runners.ssh]
    user = "root"
    identity_file = "/home/gitlab-runner/.ssh/id_rsa.gitlab"
  [runners.virtualbox]
    base_name = "c7-gitlab"
    base_snapshot = "c7-gitlab-snap-20170404"
    disable_snapshots = true

```
